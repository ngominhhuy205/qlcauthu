<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/qlcauthu',function () {
//     return view('index');
// });
Route::get('/qlcauthu','DemoController@select');
Route::get('/add','DemoController@index');

Route::post('/add','DemoController@store');
Route::get('/edit/{id}','DemoController@edit');
Route::post('edit/{id}','DemoController@update');

Route::get('/delete/{id}','DemoController@delete');