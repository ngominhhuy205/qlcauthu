<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cauthu extends Model
{
    //
    protected $table ='cauthu';
    protected $fillable=['id','ten','tuoi','quoctich','vitri','luong'];
    public $timestamp = false;
}
