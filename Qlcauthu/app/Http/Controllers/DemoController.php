<?php

namespace App\Http\Controllers;

use App\Demo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\cauthu;
class DemoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('add');
    }
    public function select()
    {
        $cauthu = cauthu::all();
        return view('index',compact('cauthu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }
    public function postcauthu(Request $request){
        echo $request->ten."-----";
        echo $request->tuoi."-----";
        echo $request->quoctich."-----";
        echo $request->vitri."-----";
        echo $request->luong;
       
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cauthu = new cauthu();
        $cauthu->ten = $request->ten;
        $cauthu->tuoi = $request->tuoi;
        $cauthu->quoctich = $request->quoctich;
        $cauthu->vitri = $request->vitri;
        $cauthu->luong = $request->luong;
        $cauthu->save();
        return redirect('/qlcauthu')->with('updated','Active successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function show(Demo $demo)
    {
        //
    }
    public function delete($id)
    {
        $cauthu = cauthu::find($id);
        $cauthu->delete();
        return redirect('/qlcauthu')->with('updated','Active successfully');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = cauthu::find($id);
        //print_r($data);
        return view('edit',compact('data'));
        
    }
    public function update(Request $request,$id)
    {   
        $cauthu = cauthu::find($id);
        $cauthu->ten = $request->ten;
        $cauthu->tuoi = $request->tuoi;
        $cauthu->quoctich = $request->quoctich;
        $cauthu->vitri = $request->vitri;
        $cauthu->luong = $request->luong;
        $cauthu->save();
        
        
       
        return redirect('/qlcauthu')->with('updated','Active successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Demo  $demo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Demo $demo)
    {
        //
    }
}
